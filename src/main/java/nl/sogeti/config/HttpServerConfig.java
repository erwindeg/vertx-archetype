package nl.sogeti.config;

import org.vertx.java.core.http.HttpServer;
import org.vertx.java.platform.Container;

public class HttpServerConfig {
	public static HttpServer startServer(Container container, HttpServer server){
		int port = Integer.parseInt(container.env().get("OPENSHIFT_VERTX_PORT") != null ? container.env().get("OPENSHIFT_VERTX_PORT") : "8080");
		String ip = container.env().get("OPENSHIFT_VERTX_IP") != null ? container.env().get("OPENSHIFT_VERTX_IP") : "127.0.0.1";

		server.listen(port, ip);
		container.logger().info("Webserver started, listening on port: " + port);
		return server;
	}
}
