package nl.sogeti.config;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;

public class MatcherConfig {
	public static RouteMatcher getStaticRoute(final String welcomePage,final String path){
		RouteMatcher matcher = new RouteMatcher();
		// Bind index.html to /
		matcher.get("/", new Handler<HttpServerRequest>() {

			@Override
			public void handle(HttpServerRequest req) {
				req.response().sendFile(path+"/"+welcomePage);
			}
		});

		// Bind static content folder app to /app
		matcher.getWithRegEx("^\\/"+path+"\\/.*", new Handler<HttpServerRequest>() {

			@Override
			public void handle(HttpServerRequest req) {
				req.response().sendFile(req.path().substring(1));
			}
		});
		return  matcher;
	}
}
