package nl.sogeti.config;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;


public class MongoConfig {
	public static JsonObject getConfig(final Container container) {
		JsonObject mongoConfig = new JsonObject();
		mongoConfig.putString("address", "mongodb-persistor");
		String host = container.env().get("OPENSHIFT_MONGODB_DB_HOST");
		mongoConfig.putString("host", host != null ? host : "localhost");
		String port = container.env().get("OPENSHIFT_MONGODB_DB_PORT");
		mongoConfig.putNumber("port", port != null ? Integer.valueOf(port) : 27017);
		mongoConfig.putString("username", container.env().get("OPENSHIFT_MONGODB_DB_USERNAME"));
		mongoConfig.putString("password", container.env().get("OPENSHIFT_MONGODB_DB_PASSWORD"));
		mongoConfig.putString("db_name", "vertxtest");
		return mongoConfig;
	}
	
	public static void deployModule(final Container container){
		container.deployModule("io.vertx~mod-auth-mgr~2.0.0-final");
		container.deployModule("io.vertx~mod-mongo-persistor~2.0.0-final",
				getConfig(container), new Handler<AsyncResult<String>>() {
					@Override
					public void handle(AsyncResult<String> arg0) {
						container.logger().info("deployed mongo module");
					}
				});
	}
}
