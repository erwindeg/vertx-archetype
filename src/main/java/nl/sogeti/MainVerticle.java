package nl.sogeti;

/*
 * Copyright 2013 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * @author <a href="http://tfox.org">Tim Fox</a>
 */

import nl.sogeti.config.HttpServerConfig;
import nl.sogeti.config.MatcherConfig;
import nl.sogeti.config.MongoConfig;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.platform.Verticle;

/*
 *
 */
public class MainVerticle extends Verticle {

	public void start() {
		MongoConfig.deployModule(container);
		RouteMatcher matcher = MatcherConfig.getStaticRoute("index.html", "app");
		HttpServer server = vertx.createHttpServer().requestHandler(matcher);

		matcher.get("/api/hello-world", new Handler<HttpServerRequest>() {

			@Override
			public void handle(HttpServerRequest request) {
				request.response().putHeader("Content-Type", "application/json");
				request.response().end("{\"content\" : \"Hello world!\" }");
			}
		});
		
		HttpServerConfig.startServer(container, server);
	}

}
